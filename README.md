# Test assignment: multi-line chart with d3.js and Angular (backend part)

## Overview

This project shows how to create a multi-line chart using D3.js and Angular. The chart displays company's financial indicators for July'18. The data is taken from the DB through API GET request.

## Get started

To run the server follow the steps below:

1. download MongoDB and Robo 3T
2. run `mongod` at the prompt
3. at the prompt type `mongoimport --db technoserve --collection raw_data --type csv --headerline  --file PATH_TO_THE_FILE` (PATH_TO_THE_FILE describes a path to a .csv file with the data)
4. set the connection in Robo 3T, navigate to New Connection/technoserve/Collections/raw_data and run the code
    `db.getCollection('raw_data').find({});`<br />

    `db.raw_data.find().forEach(function(doc) {`<br />
        `doc.dttm = new ISODate(doc.dttm);`<br />
        `doc.modify_dttm = new ISODate(doc.modify_dttm);`<br />
        `db.raw_data.save(doc)`<br />
    `});`<br />

5. clone the repo `git clone git clone https://olgakozhevnikova@bitbucket.org/olgakozhevnikova/d3-angular-project-backend.git`
6. navigate to the folder `cd d3-angular-project-backend`
7. install node modules `npm install`
8. run `node src` to start the server
