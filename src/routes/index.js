const cors = require("cors");

const dataRoutes = require("./DataRoutes");

class Routes {
  init(app) {

    app.use(dataRoutes);
    
    // Catch all the mismatch routes
    app.get("/*", this.notFound);
    app.post("/*", this.notFound);
    app.put("/*", this.notFound);
    app.delete("/*", this.notFound);
    
    // CORS cross-origin resource sharing (because frontend and backend are on different servers)
    app.use(cors);
  }

  notFound(req, res) {
    return res.status(404).json({
      error: true,
      message: "This api does not exist"
    });
  }
}

module.exports = Routes;
