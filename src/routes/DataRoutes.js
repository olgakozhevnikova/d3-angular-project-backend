const Express = require("express");
const router = new Express.Router();

const DataController = require('../controllers/DataController');
const controller = new DataController();

router.get('/data', controller.index);

module.exports = router;
