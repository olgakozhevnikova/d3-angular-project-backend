'use strict';

require('dotenv').config();

const Express = require('express');

const Routes = require('./routes');
const Database = require('./services/Database');

// App
const PORT = process.env.PORT || 3000;
const app = Express();


process.on('uncaughtException', (err) => {
  console.error('Uncaught Exception: ', err)
})
process.on('unhandledRejection', (reason, p) => {
  console.error('Unhandled Rejection: Promise:', p, 'Reason:', reason)
})

function start() {
  
  const db = new Database();
  const routes = new Routes();
  
  db.init();
  routes.init(app);

	// Start the application
  app.listen(PORT, () => {
  console.log(`Server enviroment: ${process.env.NODE_ENV}`);
    console.log(`Server is up: http://localhost:${PORT}`);
	});
}



start();