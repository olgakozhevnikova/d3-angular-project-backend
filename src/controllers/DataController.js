const dayjs = require("dayjs");
const Data = require("../models/Data");

class DataController {
  async index(req, res) {
    let { fields, range, step, start_date_time, end_date_time } = req.query;
    
    let $project = {};
    const $match = {};

    const allowedSteps = ["1m", "15m", "30m", "1h"];
    if (allowedSteps.indexOf(step) === -1) {
      step = "15m";
    }

    // Range validation | If not from input.
    const allowedRangesMap = {
      "4h": [4, "hours"],
      "24h": [24, "hours"],
      "48h": [48, "hours"],
      "7d": [7, "days"],
      "30d": [30, "days"]
    };

    // Default is 48h
    if (Object.keys(allowedRangesMap).indexOf(range) === -1) {
      range = "48h";
    }

    // Get the toDate (ending entry). If not, select the current date
    const toDate = end_date_time ? new Date(end_date_time) : new Date();
    // Get the fromDate (starting entry). If not, then with the help of range, figure it out
    const fromDate = start_date_time ? new Date(start_date_time) : dayjs(toDate).subtract(
      allowedRangesMap[range][0],
      allowedRangesMap[range][1]
    );

    // Add query filter to get data
    $match["$and"] = [
      { dttm: { $gte: fromDate } },
      { dttm: { $lte: toDate } }
    ];

    // We may need data for few options only
    if (fields) {
      const _fields = fields.split(",");
      _fields.forEach(x => ($project[x] = 1));
    } else {
      $project = {
        dm_ui_chart_1m_fact_value: 1,
        dm_ui_chart_1m_plan_value: 1,
        dm_ui_chart_1m_forecast_upd: 1,
        dm_ui_chart_1m_forecast_24: 1,
        dm_ui_chart_1m_forecast_3: 1,
        dm_ui_chart_1m_ved: 1,
        dm_ui_chart_1m_mae_24: 1,
        dm_ui_chart_1m_mae_3: 1
      };
    }
    // We are also selecting dttm for X-axis
    $project["_id"] = 0;
    $project["dttm"] = 1;

    // Prepare the mongo aggregate pipeline
    const aggregate = [{ $match }, { $project }];
    console.log(JSON.stringify(aggregate))

    // Query
    const data = await Data.aggregate(aggregate);

    // Data
    return res.json(data);
  }
}

module.exports = DataController;
