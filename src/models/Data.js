const mongoose = require("mongoose");

const dataModel = new mongoose.Schema({
  dttm: Date,
  dm_ui_chart_1m_fact_value: Number,
  dm_ui_chart_1m_plan_value: Number,
  dm_ui_chart_1m_forecast_upd: Number,
  dm_ui_chart_1m_forecast_24: Number,
  dm_ui_chart_1m_forecast_3: Number,
  dm_ui_chart_1m_ved: Number,
  dm_ui_chart_1m_mae_24: Number,
  dm_ui_chart_1m_mae_3: Number,
  process_run_id: Number,
  modify_dttm: Date,
}, {
  strict: false,
  collection: 'raw_data'
});

module.exports = mongoose.model("Data", dataModel);
